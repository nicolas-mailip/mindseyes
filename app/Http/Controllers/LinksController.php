<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Link;
use \Input;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class LinksController extends Controller {

	public function create()
	{
		return view('links.create');
	}

	public function store(){
		$url = Input::get('url');
		$link = Link::firstOrCreate(['url' => $url]);
		return view('links.success', compact('link'));
	}

	public function show($id)
	{
		$link = Link::findorFail($id);
		return new RedirectResponse($link->url);
	}

}
