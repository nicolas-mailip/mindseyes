<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
class IpFilter {
	private $ip;

	public function __construct($ip){
		$this->ip = $ip;
	}
}

App::bind('IpFilter', function($app){
	return new IpFilter($app->make('request')->getClientIp());
});


Route::get('/', function(IpFilter $ipfilter){
	dd($ipfilter);
});

Route::resource('link', 'LinksController');

//Route::get('/links/create', 'LinksController@create');
//Route::post('/links/create', 'LinksController@store');
//Route::get('/r/{id}', 'LinksController@show')->where('id', '[0-9]+');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
